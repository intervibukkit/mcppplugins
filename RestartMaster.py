import threading
from mcpp.modules.mcpp_plug_req import Plugin, ServPlugin
from mcpp.modules.mcpp_enums import EventAction, ReqResult
from mcpp.modules.mcpp_plug_utils import PluginUtils


class Main(Plugin, ServPlugin):
    def __init__(self, api):
        Plugin.__init__(self, api)
        self.api = api
        self.AUTHOR = 'InterVi'
        self.DESCRIPTION = 'Перезапуск серверов в заданное время.'
        self.NAME = 'RestartMaster'
        self.VERSION = '1.2'
        self.th = {}
        """Словарь с потоками (поток, Event), ключи - названия."""
        self.p = PluginUtils(self.NAME)
        """PluginUtils."""
        self.deny = self.__config()
        """bool, при True плагин не загружается."""

    def __config(self):
        """Чтение конфига.

        :return: True, если конфиг был создан
        """
        if self.p.is_config():
            self.p.read_config()
            return False
        else:
            self.p.config['MAIN'] = {'enable': 'yes'}
            self.p.config['RESTART'] = {'SERVER': '04:00:00'}
            self.p.config['STOP'] = {'TWO_SERVER': '03:00:00'}
            self.p.save_config()
            return True

    def __add_task(self, time, name, stop=False):
        """Запуск потока для сервера. Потоки добавляются в th.

        :param time: время срабатвания (строка)
        :param name: имя сервера
        :param stop: bool, True - остановить, False - перезапустить
        """
        def rest():
            """Перезапуск сервера."""
            with self.api.mcppserver.rlock:
                serv = self.api.mcppserver.sm.servs[name]
                try:
                    serv.rlock.acquire()
                    mess = '[RestartMaster]: RESTART'
                    serv.log.warning(mess)
                    with self.api.module['grlock']:
                        self.api.module['debug'].info(mess + ' ' + name)
                    serv.stop()
                    serv.wait_run(
                        serv.SERV, name)
                finally:
                    serv.rlock.release()

        def stop_():
            """Остановка сервера."""
            with self.api.mcppserver.rlock:
                serv = self.api.mcppserver.sm.servs[name]
                try:
                    serv.rlock.acquire()
                    mess = '[RestartMaster]: STOP'
                    serv.log.warning(mess)
                    with self.api.module['grlock']:
                        self.api.module['debug'].info(mess + ' ' + name)
                finally:
                    serv.rlock.release()
                self.api.mcppserver.sm.stop(name)

        # у каждого потока свое событие, чтобы остановить его
        event = threading.Event()
        if stop:
            t = threading.Thread(
                target=PluginUtils.time_task, args=(time, stop_, event),
                name=name)
        else:
            t = threading.Thread(
                target=PluginUtils.time_task, args=(time, rest, event),
                name=name)
        t.start()
        self.th[name] = (t, event)

    def load(self, api):
        """Добавление плагина в ServManager для прослушки событий.

        :return: None или EventAction.deny (если плагин отключен в конфиге)
        """
        if self.deny:
            with self.api.module['grlock']:
                self.api.module['debug'].warning(
                    'Конфиг создан, отредактируйте и перезагрузите плагин.')
            return EventAction.deny
        if self.p.config['MAIN']['enable'] != 'yes':
            return EventAction.deny
        self.api.pmanager.add_sm_plugin(self)

    def __clear(self):
        """Остановка потоков."""
        for t in self.th:
            self.th[t][1].set()
        self.th.clear()

    def __start_th(self, name):
        """Запуск потока для сервера.

        :param name: имя сервера
        """
        if name in self.th: return
        if name.lower() in self.p.config['RESTART']:
            self.__add_task(self.p.config['RESTART'][name], name)
        elif name.lower() in self.p.config['STOP']:
            self.__add_task(self.p.config['STOP'][name], name, True)

    def __start(self):
        """Запуск потоков для уже запущенных серверов."""
        for s in self.api.mcppserver.sm:
            self.__start_th(s)

    def unload(self):
        """Остановка потоков и удаление себя из ServManager."""
        self.__clear()
        self.api.pmanager.del_sm_plugin(self)

    def reload(self):
        """Перезагрузка конфига.

        :return: ReqResult.good
        """
        self.__config()
        self.__clear()
        return ReqResult.good

    def start(self, name, result, action):
        """Запуск потока при запуске нового сервера.

        :param name: имя сервера
        :param result: EventAction
        :param action: EventAction
        :return: None
        """
        if result and action != EventAction.deny:
            self.__start_th(name)

    def stop(self, name, result, action):
        """Остановка потока при остановке сервера.

        :param name: имя сервера
        :param result: EventAction
        :param action: EventAction
        :return: None
        """
        if result and action != EventAction.deny:
            if name in self.th:
                self.th[name][1].set()
                del self.th[name]
