import logging
import re
from mcpp.modules.mcpp_plug_req import Plugin, ServPlugin, ContainerPlugin
from mcpp.modules.mcpp_enums import EventAction, ReqResult
from mcpp.modules.mcpp_plug_utils import PluginUtils


class Main(Plugin, ServPlugin):
    def __init__(self, api):
        Plugin.__init__(self, api)
        self.api = api
        self.AUTHOR = 'InterVi'
        self.DESCRIPTION = 'Сбор статистики.'
        self.NAME = 'StatMaster'
        self.VERSION = '1.0'
        self.p = PluginUtils(self.NAME)
        """PluginUtils."""
        self.deny = self.__config()
        """bool, при True плагин не загружается."""
        self.cp = CPlugin(self.p.config)
        """CPlugin."""

    def __config(self):
        """Чтение конфига.

        :return: True, если конфиг был создан
        """
        if self.p.is_config():
            self.p.read_config()
            return False
        else:
            self.p.config['MAIN'] = {
                'enable': 'yes',
                'only_24h': 'yes'
            }
            self.p.config['STATS'] = {
                'joins': 'yes',
                'newbie': 'yes',
                'ips': 'yes',
                'max_online': 'yes',
                'stops': 'yes',
                'coms': 'yes',
                'chat': 'yes'
            }
            self.p.config['LANGS'] = {
                'joins': 'Игроков зашло: $joins',
                'newbie': 'Новых игроков: $newbie',
                'ips': 'Уникальных IP: $ips',
                'newips': 'Новых IP: $newips'
            }
            self.p.config['REGEX'] = {
                'joins': '',
                'newbie': '',
                'ips': ''
            }
            self.p.save_config()
            return True

    def load(self, api):
        """Добавление плагина в ServManager для прослушки событий.

        :return: None или EventAction.deny (если плагин отключен в конфиге)
        """
        if self.deny:
            with self.api.module['grlock']:
                self.api.module['debug'].warning(
                    'Конфиг создан, отредактируйте и перезагрузите плагин.')
            return EventAction.deny
        if self.p.config['MAIN']['enable'] != 'yes':
            return EventAction.deny
        self.api.pmanager.add_sm_plugin(self)

    def unload(self):
        """Удаление себя из ServManager."""
        self.api.pmanager.del_sm_plugin(self)

    def reload(self):
        """Перезагрузка конфига.

        :return: ReqResult.good
        """
        self.__config()
        return ReqResult.good

    def start(self, name, result, action):
        """Добавление себя в Container.

        :param name: имя сервера
        :param result: EventAction
        :param action: EventAction
        :return: None
        """
        if result != EventAction.deny:
            self.api.pmanager.add_serv_plugin(self.cp, name)
            pass

    def stop(self, name, result, action):
        """Удаление себя из Container.

        :param name: имя сервера
        :param result: EventAction
        :param action: EventAction
        :return: None
        """
        if result != EventAction.deny:
            self.api.pmanager.del_serv_plugin(self.cp, name)
            pass


class CPlugin(ContainerPlugin):
    def __init__(self, conf):
        ContainerPlugin.__init__(self)
        self.NAME = 'StatMaster'
        self.conf = conf

    def stop_server(self):
        pass

    def out(self, mess, action):
        pass
